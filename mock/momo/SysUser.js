
export default [
  // 用户修改密码
  {
    url: '/platform/sysUser/sysUserPwd/v1',
    type: 'post',
    response: config => {
      return {
        'data': '用户修改密码成功',
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 角色给用户(授权)
  {
    url: '/platform/role/rolesToUser/v1',
    type: 'post',
    response: config => {
      return {
        'data': '为用户授权角色成功',
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 角色给用户(回显)
  {
    url: '/platform/role/userCheckRoles/v1',
    type: 'post',
    response: config => {
      return {
        'data': {
          'checkList': [],
          'createBy': '',
          'createTime': null,
          'delFlag': null,
          'disabled': false,
          'disabledFlag': null,
          'groupId': null,
          'id': null,
          'idStr': '',
          'remark': '',
          'roles': [{
            'checkList': [],
            'createBy': '李杰',
            'createTime': '2019-08-19 18:49:14',
            'delFlag': 0,
            'disabled': false,
            'disabledFlag': 1,
            'groupId': 1,
            'id': 194913401527472128,
            'idStr': '194913401527472128',
            'remark': '',
            'roles': [],
            'sysRoleName': '测试角色',
            'sysRoleType': 2,
            'updateBy': '李杰',
            'updateTime': '2019-08-19 18:58:22',
            'uuid': '9d287ba9c807413e83a62f9dc450d7c1'
          }],
          'sysRoleName': '',
          'sysRoleType': null,
          'updateBy': '',
          'updateTime': null,
          'uuid': ''
        },
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 变更用户状态
  {
    url: '/platform/sysUser/sysUserStatus/v1',
    type: 'post',
    response: config => {
      return {
        'data': '用户状态设置成功',
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 用户编辑
  {
    url: '/platform/sysUser/sysUserModify/v1',
    type: 'post',
    response: config => {
      return {
        'data': '编辑用户成功',
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // uuid 查询用户详情
  {
    url: '/platform/sysUser/sysUserDetail/v1',
    type: 'post',
    response: config => {
      return {
        'data': {
          'createBy': '李杰',
          'createTime': '2019-08-06 22:53:51',
          'delFlag': 0,
          'disabledFlag': 0,
          'groupId': 1,
          'id': null,
          'remark': '我是备注',
          'sysUserArea': null,
          'sysUserAreaName': '',
          'sysUserCity': null,
          'sysUserCityName': '',
          'sysUserEmail': 'test@163.com',
          'sysUserName': '用户编辑',
          'sysUserPhone': '',
          'sysUserProvince': null,
          'sysUserProvinceName': '',
          'updateBy': '李杰',
          'updateTime': '2019-08-07 13:36:26',
          'uuid': '8767f0bff61847cb945d8c5567c5f37c'
        },
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 用户新增--账号密码
  {
    url: '/platform/sysUser/sysUserAdd/v1',
    type: 'post',
    response: config => {
      return {
        'data': '新增用户成功',
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },

  // 用户列表
  {
    url: '/platform/sysUser/sysUserList/v1',
    type: 'post',
    response: config => {
      return {
        'data': {
          'endRow': 0,
          'hasNextPage': false,
          'hasPreviousPage': false,
          'isFirstPage': false,
          'isLastPage': false,
          'list': [
            {
              'createBy': '',
              'createTime': '2019-04-24 21:57:54',
              'delFlag': null,
              'editButton': true,
              'disabledFlag': 0,
              'disabledFlagButtonShow': true,
              'groupId': null,
              'id': 4,
              'pwdBinding': false,
              'pwdBindingDate': null,
              'pwdBindingFlag': null,
              'pwdBindingName': '',
              'pwdButtonShow': true,
              'remark': '',
              'roleButtonShow': true,
              'roles': [
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 7,
                  'remark': '',
                  'sysRoleName': '测试新增角色',
                  'sysRoleType': 1,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                }
              ],
              'sysUserArea': null,
              'sysUserAreaName': '',
              'sysUserCity': null,
              'sysUserCityName': '',
              'sysUserEmail': '1@qq.com',
              'sysUserName': 'aaaa',
              'sysUserPhone': '1',
              'sysUserProvince': null,
              'sysUserProvinceName': '',
              'updateBy': '李杰',
              'updateTime': null,
              'uuid': '7575a0f64189451ba6b7aac8da797a83',
              'wxBinding': false,
              'wxBindingDate': null,
              'wxBindingFlag': null,
              'wxBindingName': ''
            },
            {
              'createBy': '',
              'createTime': '2019-04-23 21:21:05',
              'delFlag': null,
              'editButton': true,
              'disabledFlag': 1,
              'disabledFlagButtonShow': true,
              'groupId': null,
              'id': 3,
              'pwdBinding': false,
              'pwdBindingDate': null,
              'pwdBindingFlag': null,
              'pwdBindingName': '',
              'pwdButtonShow': true,
              'remark': '',
              'roleButtonShow': true,
              'roles': [],
              'sysUserArea': null,
              'sysUserAreaName': '',
              'sysUserCity': null,
              'sysUserCityName': '',
              'sysUserEmail': 'a@aa.com',
              'sysUserName': '啊啊',
              'sysUserPhone': 'a',
              'sysUserProvince': null,
              'sysUserProvinceName': '',
              'updateBy': '李杰',
              'updateTime': null,
              'uuid': 'd04b26d876574a3998917e65739ac984',
              'wxBinding': false,
              'wxBindingDate': null,
              'wxBindingFlag': null,
              'wxBindingName': ''
            },
            {
              'createBy': '',
              'createTime': '2019-04-21 12:20:18',
              'delFlag': null,
              'editButtonShow': true,
              'disabledFlag': 0,
              'disabledFlagButtonShow': true,
              'groupId': null,
              'id': 2,
              'pwdBinding': false,
              'pwdBindingDate': null,
              'pwdBindingFlag': null,
              'pwdBindingName': '',
              'pwdButtonShow': true,
              'remark': '',
              'roleButtonShow': true,
              'roles': [
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 7,
                  'remark': '',
                  'sysRoleName': '测试新增角色',
                  'sysRoleType': 1,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                }
              ],
              'sysUserArea': null,
              'sysUserAreaName': '',
              'sysUserCity': null,
              'sysUserCityName': '',
              'sysUserEmail': '15050451501@163.com',
              'sysUserName': 'test',
              'sysUserPhone': '15050451501',
              'sysUserProvince': null,
              'sysUserProvinceName': '',
              'updateBy': '李杰',
              'updateTime': null,
              'uuid': '691d837480824acfa6aad086898026d0',
              'wxBinding': false,
              'wxBindingDate': null,
              'wxBindingFlag': null,
              'wxBindingName': ''
            },
            {
              'createBy': '',
              'createTime': '2019-01-11 15:09:26',
              'delFlag': null,
              'editButtonShow': true,
              'disabledFlag': 0,
              'disabledFlagButtonShow': true,
              'groupId': null,
              'id': 1,
              'pwdBinding': true,
              'pwdBindingDate': '2019-07-30 16:28:37',
              'pwdBindingFlag': 1,
              'pwdBindingName': 'momo',
              'pwdButtonShow': true,
              'remark': '',
              'roleButtonShow': true,
              'roles': [
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 2,
                  'remark': '',
                  'sysRoleName': '测试角色',
                  'sysRoleType': 1,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                },
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 3,
                  'remark': '',
                  'sysRoleName': '测试角q色',
                  'sysRoleType': 0,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                },
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 4,
                  'remark': '',
                  'sysRoleName': '腾讯爸爸管理员',
                  'sysRoleType': 0,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                },
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 5,
                  'remark': '',
                  'sysRoleName': '普通员工1',
                  'sysRoleType': 1,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                },
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 6,
                  'remark': '',
                  'sysRoleName': 'MOMO',
                  'sysRoleType': 0,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                },
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 7,
                  'remark': '',
                  'sysRoleName': '测试新增角色',
                  'sysRoleType': 1,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                },
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 8,
                  'remark': '',
                  'sysRoleName': '新版本角色新增',
                  'sysRoleType': 1,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                },
                {
                  'createBy': '',
                  'createTime': null,
                  'delFlag': null,
                  'disabledFlag': null,
                  'groupId': null,
                  'id': 9,
                  'remark': '',
                  'sysRoleName': 'aa',
                  'sysRoleType': 1,
                  'updateBy': '',
                  'updateTime': null,
                  'uuid': ''
                }
              ],
              'sysUserArea': null,
              'sysUserAreaName': '',
              'sysUserCity': null,
              'sysUserCityName': '',
              'sysUserEmail': '15050451502@163.com',
              'sysUserName': '李杰',
              'sysUserPhone': '15050451502',
              'sysUserProvince': null,
              'sysUserProvinceName': '',
              'updateBy': '李杰',
              'updateTime': null,
              'uuid': 'add6df8ac7f84211ba2303630fca10db',
              'wxBinding': false,
              'wxBindingDate': null,
              'wxBindingFlag': null,
              'wxBindingName': ''
            }
          ],
          'navigateFirstPage': 0,
          'navigateLastPage': 0,
          'navigatePages': 0,
          'navigatepageNums': [],
          'nextPage': 0,
          'pageNum': 1,
          'pageSize': 20,
          'pages': 0,
          'prePage': 0,
          'size': 0,
          'startRow': 0,
          'total': 4
        },
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  }
]
